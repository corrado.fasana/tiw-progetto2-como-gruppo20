//Gestisce gli eventi che provengono dal server mediante SSE
function registerSSE(CRManager, IRManager)
{
    var source = new EventSource('SendSSEUpdate');

    //Quando l'evento riguarda la creazione di una riunione, viene richiesto al CRManager di aggiornare la tabella delle riunioni create
    source.addEventListener('reunionCreated',function (e){
        CRManager.show(JSON.parse(e.data));
    },true);

    //Quando l'evento riguarda l'invito a una riunione, viene aggiornata la tabella degli inviti alle riunioni
    source.addEventListener('reunionInvitation',function (e){
        IRManager.show(JSON.parse(e.data));
    },true);

    //Quando viene ricevuto un errore, non serve fare nulla: la modalità real-time non funziona
    source.addEventListener('error',function (e){
    },true);

    var closeFunction = function(){
        source.close();
    }

    return closeFunction;
}
