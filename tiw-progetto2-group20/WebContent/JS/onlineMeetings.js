(function(){
    //Il pageManager si occupa di inizializzare lo stato della pagina e creare gli altri gestori
    var pageManager = new PageManager();

    //Al caricamento della pagina...
    window.addEventListener("load", () => {
      //...se un utente è loggato, allora si procede, altrimenti si reindirizza il client alla pagina di login/registrazione
      if(sessionStorage.getItem("username")===null)
        window.location.href = "index.html";
      else
          pageManager.start();
    }, false);

    //Si occupa di mostrare il messaggio di benvenuto
    function WelcomeManager(welcomeContainer, username){
        this.show = function(){
            welcomeContainer.textContent="Bentornato "+username+"!";
        }
    }

    //Si occupa di gestire i componenti della pagina che riguardano le riunioni
    function ReunionsManager(elements){
        //Effettua una chiamata asincrona al server per ottenere l'elenco delle riunioni
        this.retrieveReunions=function(){
            var self = this;
            asyncCall("GET", elements["urlRequest"], null,
                function(request) {
                  if (request.readyState === XMLHttpRequest.DONE) {
                    var message = request.responseText;
                    //Se l'operazione ha successo, le riunioni trovate verranno mostrate all'utente
                    if (request.status === HttpCodes.SUCCESS) {
                      self.show(JSON.parse(request.responseText));
                      elements["errorContainer"].style.display="none";
                    }
                    //Altrimenti se la sessione è scaduta
                    else if(request.status === HttpCodes.UNAUTHORIZED){
                        closeSession();
                    }
                    else {    //Altrimenti, l'errore restituito dal server viene mostrato
                      elements["errorContainer"].textContent = message;
                      elements["errorContainer"].style.display="block";
                    }
                  }
                }
              );
        }

        //Reimposta il componente allo stato iniziale
        this.reinitialize = function(){
            elements["tBody"].innerHTML = "";
            elements["table"].style.display="none";
            elements["emptyContainer"].style.display="none";
        }

        //Mostra l'elenco delle riunioni sulla pagina
        this.show=function(reunions){
            var newRow,titleCell,dateCell, durationCell, maxParticipantsCell;
            this.reinitialize();
            //Se non ci sono riunioni, viene mostrato un div contente un messaggio informativo
            if(reunions.length === 0){
                elements["emptyContainer"].textContent=elements["emptyMessage"];
                elements["emptyContainer"].style.display="block";
            }
            //Altrimenti si procede alla costruzione della tabella e alla sua visualizzazione
            else{
                reunions.forEach((reunion) => {
                    newRow = document.createElement("tr");
                    titleCell = document.createElement("td");
                    titleCell.textContent = reunion.title;
                    newRow.appendChild(titleCell);
                    dateCell = document.createElement("td");
                    dateCell.textContent = reunion.dateTime;
                    newRow.appendChild(dateCell);
                    durationCell = document.createElement("td");
                    durationCell.textContent = reunion.duration;
                    newRow.appendChild(durationCell);
                    maxParticipantsCell = document.createElement("td");
                    maxParticipantsCell.textContent = reunion.maxParticipants;
                    newRow.appendChild(maxParticipantsCell);
                    elements["tBody"].appendChild(newRow);
                });
                elements["table"].style.display="block";
            }
        }
    }

    //Si occupa di gestire la form di creazione di una nuova riunione
    function CreationManager(submit,errorContainer,modalManager){
        //Definisce data e ora attuali (rispetto all'orale locale)...
        var now = new Date();
        now.setMinutes(now.getMinutes() - now.getTimezoneOffset());

        //...e le imposta come valore minimo del campo dataOra per la creazione di una riunione (non possono essere create riunioni nel passato)
        var formattedDate = now.toISOString().substring(0,16);
        submit.closest("form").querySelector("input[type=datetime-local]").setAttribute("min", formattedDate);

        //Registra l'evento di click sul submit per l'inoltro della form
        this.registerEvents = function(){
            submit.addEventListener('click', (e) => {
              var form = submit.closest("form");

              if (form.checkValidity()) {
                /*Se i dati inseriti sono validi, viene comunque effettuata una chiamata al server per informarlo della richiesta di creazione di una nuova
                  riunione. Se anche il server valida i dati, il numero massimo di partecipanti viene memorizzato lato client per i controlli successivi*/
                asyncCall("POST", 'CheckReunionParameters', form,
                  function(request) {
                    if (request.readyState === XMLHttpRequest.DONE) {
                        var message = request.responseText;
                        //Se l'operazione ha successo, si salva il numero massimo di partecipanti
                        if(request.status===HttpCodes.SUCCESS)
                        {
                            errorContainer.style.display="none";
                            sessionStorage.setItem("maxParticipants",message);
                            modalManager.retrieveUsers();
                        }
                        //Altrimenti se la sessione è scaduta
                        else if(request.status === HttpCodes.UNAUTHORIZED){
                            closeSession();
                        }
                        else{   //Altrimenti viene mostrato un errore
                            errorContainer.textContent=message;
                            errorContainer.style.display="block";
                        }
                    }
                  }
                );
              } else {
                  form.reportValidity();
              }
          });
        }
    }

    //Si occupa della gestione della pagina modale
    function ModalPageManager(modalContainer,errorContainer,createdReunionsManager){
        var tBody = document.getElementById("tBodyUsersSelection");
        var modalError = document.getElementById("errorModal");

        //Imposta lo stato iniziale del componente
        this.reinitialize = function(){
            modalContainer.style.display="none";
            tBody.innerHTML="";
            modalError.style.display="none";
            tBody.closest("table").style.display="block";   //Necessario perchè di default le tabelle sono tutte nascoste
        }

        //Effettua una chiamata asincrona al server per ottenere l'elenco degli utenti
        this.retrieveUsers = function(){
            var self = this;
            asyncCall("GET", "GetUsers", null,
                function(request) {
                  if (request.readyState === XMLHttpRequest.DONE) {
                    var message = request.responseText;
                    //Se la risposta ha successo, il numero di tentativi viene settato a 0 e gli utenti trovati vengono mostrati
                    if (request.status === HttpCodes.SUCCESS) {
                      self.show(JSON.parse(request.responseText));
                      sessionStorage.setItem('attempt', 0);
                    }
                    //Altrimenti se la sessione è scaduta
                    else if(request.status === HttpCodes.UNAUTHORIZED){
                        closeSession();
                    }
                    else {    //Altrimenti, si mostra l'errore ricevuto
                      errorContainer.textContent = message;
                      errorContainer.style.display="block";
                    }
                  }
                }
              );
        }

        //Si occupa di mostrare l'elenco degli utenti
        this.show = function(users){
            var newRow,nameCell,surnameCell, emailCell, usernameCell, checkBoxCell, checkBoxElement;
            this.reinitialize();
            //Se non sono presenti utenti, oltre a quello loggato, non si consente la creazione della riunione e viene semplicemente mostrato un errore
            if(users.length === 0){
                errorContainer.textContent="Nessun utente disponibile per creare la riunione. Riprovare più tardi."
                errorContainer.style.display="block";
            }
            //Altrimenti, si crea e si mostra la pagina modale
            else{
                users.forEach((user) => {
                    newRow = document.createElement("tr");
                    nameCell = document.createElement("td");
                    nameCell.textContent = user.name;
                    newRow.appendChild(nameCell);
                    surnameCell = document.createElement("td");
                    surnameCell.textContent = user.surname;
                    newRow.appendChild(surnameCell);
                    usernameCell = document.createElement("td");
                    usernameCell.textContent = user.username;
                    newRow.appendChild(usernameCell);
                    emailCell = document.createElement("td");
                    emailCell.textContent = user.email;
                    newRow.appendChild(emailCell);
                    checkBoxCell = document.createElement("td");
                    checkBoxElement = document.createElement("input");
                    checkBoxElement.type = "checkbox";
                    checkBoxElement.name = "selectedUsers";
                    checkBoxElement.value = user.id;
                    //Viene registrato un evento sulle checkbox per colorare lo sfondo delle righe relative ai possibili invitati
                    checkBoxElement.addEventListener("change", (e) => {
                        if (e.target.checked) {
                            e.target.closest("tr").style.backgroundColor = "#33ff99";
                        } else {
                            e.target.closest("tr").style.backgroundColor = "#aaa";
                        }
                    }, false);
                    checkBoxCell.appendChild(checkBoxElement);
                    newRow.appendChild(checkBoxCell);
                    tBody.appendChild(newRow);
                });
                modalContainer.style.display="block";

            }
        }

        //Si occupa di definire gli eventi di click sui bottoni della pagina modale
        this.registerEvents = function(){
            //Evento di invio della form
            document.getElementById("btnSubmitUsersSelection").addEventListener('click', (e) => {
              var form = e.target.closest("form");

              //Controlla se il numero di tentativi memorizzato nel sessionStorage è un numero: se non lo è, non si manda nulla al server e si segnala un errore
              if(Number.isNaN(Number(sessionStorage.getItem("attempt")))){
                  errorContainer.textContent="Errore: il numero di tentativi è NaN. Riprovare più tardi";
                  errorContainer.style.display="block";
                  return;
              }

              //Si ottengono tutte le checkbox e su verifica quante sono checked
              var allCheckbox = form.querySelectorAll("input[type=checkbox]");
              var selectedUsers = 0;

              allCheckbox.forEach((checkbox) => {
                  if(checkbox.checked) selectedUsers+=1;
              });

              //Se il numero di partecipanti è 0, non si manda nulla al server e si mostra un errore
              if(selectedUsers===0){
                  modalError.textContent="Selezionare almeno un partecipante";
                  modalError.style.display="block";
                  return;
              }
              //Se il numero di utenti selezionati supera il massimo, non si manda nulla al server e si mostra un errore
              if(selectedUsers>Number(sessionStorage.getItem("maxParticipants"))){
                  //Si incrementa il numero di tentativi
                  sessionStorage.setItem("attempt",Number(sessionStorage.getItem("attempt"))+1);
                  //Controlla se il valore memorizzato di tentativi è >=3. In tal caso, non si manda nulla al server: la riunione non può essere creata
                  if(Number(sessionStorage.getItem("attempt"))>=3){
                      errorContainer.textContent="Tre tentativi di definire una riunione con troppi partecipanti, la riunione non sarà creata";
                      errorContainer.style.display="block";
                      //Viene scatenato l'evento di cancellazione per annullare l'operazione
                      var cancelEvent = new Event('click');
                      document.getElementById("btnCancelUsersSelection").dispatchEvent(cancelEvent);
                      return;
                  }
                  else{
                      modalError.textContent="Troppi utenti selezionati, eliminane almeno "+(selectedUsers-Number(sessionStorage.getItem("maxParticipants")));
                      modalError.style.display="block";
                      return;
                  }
              }
              else if (form.checkValidity()) {
                //Se tutti i campi sono validi e le precondizioni sono rispettate si invia la richiesta al server di creare la riunione vera e propria
                asyncCall("POST", 'CheckNumberOfParticipants', form,
                  function(request) {
                    if (request.readyState === XMLHttpRequest.DONE) {
                        var message = request.responseText;
                        switch (request.status) {
                          //Se l'operazione ha successo, si aggiorna l'elenco delle riunioni
                          case HttpCodes.SUCCESS:
                            createdReunionsManager.retrieveReunions();
                            errorContainer.style.display="none";
                            //Causo l'evento di cancellazione dell'operazione. E' un'operazione fittizia per svuotare la form di creazione della riunione ed
                            //eliminare gli item dal sessionStorage
                            var cancelEvent = new Event('click');
                            document.getElementById("btnCancelUsersSelection").dispatchEvent(cancelEvent);
                            break;
                          //Se la sessione è scaduta
                          case HttpCodes.UNAUTHORIZED:
                            closeSession();
                            break;
                          //Parametri passati non ben formattati
                          case HttpCodes.BAD_REQUEST:
                          //Il numero di partecipanti eccede il massimo (N.B. controllato anche lato server) o è 0
                          case HttpCodes.FORBIDDEN:
                            modalError.textContent = message;
                            modalError.style.display="block";
                            break;
                          //L'utente ha esaurito il n° di tentativi a disposizione: si annulla la procedura
                          case HttpCodes.RESET_CONTENT:
                            errorContainer.textContent="Tre tentativi di definire una riunione con troppi partecipanti, la riunione non sarà creata";
                            errorContainer.style.display="block";
                            //L'operazione viene annullata scatendando l'evento di cancellazione
                            var cancelEvent = new Event('click');
                            document.getElementById("btnCancelUsersSelection").dispatchEvent(cancelEvent);
                            break;
                          //Il server ha riscontrato qualche errore e non può creare la riunione
                          case HttpCodes.SERVER_ERROR:
                            errorContainer.textContent=message;
                            errorContainer.style.display="block";
                            //L'operazione viene annullata scatendando l'evento di cancellazione
                            var cancelEvent = new Event('click');
                            document.getElementById("btnCancelUsersSelection").dispatchEvent(cancelEvent);
                            break;
                        }
                    }
                  });
                  } else {
                  form.reportValidity();
              }
            });

            //Evento di cancellazione dell'operazione
            document.getElementById("btnCancelUsersSelection").addEventListener('click', (e) => {
              //La form viene resettata e i parametri di sessionStorage cancellati
              this.reinitialize();
              document.querySelector("body>form").reset();
              sessionStorage.removeItem("attempt");
              sessionStorage.removeItem("maxParticipants");
            });

            //Evento di click...
            window.addEventListener('click',(e) => {
              //...al di fuori della modal page
              if (e.target == document.getElementById("divModalPage")) {
                /*Torna a mostrare la form senza cancellare i dati per consentire all'utente di modificarli senza dover cancellare la riunione e rieffettuare
                  la procedura*/
                this.reinitialize();
              }
          });
        }
    }

    //Si occupa di definire lo stato iniziale della pagina
    function PageManager(){
        this.start = function(){
            //Ottenimento dei riferimenti agli elementi principali della form
            var welcomeContainer = document.getElementById("h1Welcome");
            var createdReunionsTable = document.getElementById("tableCreatedReunions");
            var createdReunionsTBody = document.getElementById("tBodyCreatedReunions");
            var errorDiv = document.getElementById("divError");
            var noCreatedReunionsDiv = document.getElementById("divNoCreatedReunions");
            var invitedReunionsTable = document.getElementById("tableInvitedReunions");
            var invitedReunionsTBody = document.getElementById("tBodyInvitedReunions");
            var noInvitedReunionsDiv = document.getElementById("divNoInvitedReunions");
            var submitFormCreation = document.getElementById("btnSubmitReunionCreation");
            var modalDiv = document.getElementById("divModalPage");

            //Creazione dei vari gestori dei componenti della pagina
            var welcomeManager = new WelcomeManager(welcomeContainer,sessionStorage.getItem("username"));
            var createdReunionsManager = new ReunionsManager({table:createdReunionsTable,
                                                              tBody:createdReunionsTBody,
                                                              errorContainer:errorDiv,
                                                              emptyContainer:noCreatedReunionsDiv,
                                                              emptyMessage:"Nessuna riunione creata è ancora valida",
                                                              urlRequest:"GetCreatedReunions"});
            var invitedReunionsManager = new ReunionsManager({table:invitedReunionsTable,
                                                              tBody:invitedReunionsTBody,
                                                              errorContainer:errorDiv,
                                                              emptyContainer:noInvitedReunionsDiv,
                                                              emptyMessage:"Nessuna riunione a cui sei stato invitato è ancora valida",
                                                              urlRequest:"GetInvitedReunions"});
            var modalManager = new ModalPageManager(modalDiv,errorDiv,createdReunionsManager);
            var creationManager = new CreationManager(submitFormCreation,errorDiv,modalManager);

            //Procedura di inizializzazione dei vari sotto-componenti
            welcomeManager.show();
            createdReunionsManager.retrieveReunions();
            invitedReunionsManager.retrieveReunions();
            creationManager.registerEvents();
            modalManager.reinitialize();
            modalManager.registerEvents();

            //Avvio della modalità SSE per la gestione real-time dell'interfaccia
            var closeSSE = registerSSE(createdReunionsManager,invitedReunionsManager);

            document.getElementById("btnLogout").addEventListener('click',(e) => {
                sessionStorage.removeItem("attempt");
                sessionStorage.removeItem("maxParticipants");
                sessionStorage.removeItem("username");
                closeSSE();
          });
        }
    }
})()
