/**
 * Login management
 */

(function() {

  //Il PageManager si occupa di definire lo stato iniziale della pagina
  var pageManager = new PageManager();

  window.addEventListener("load", () => {
    pageManager.start();
  }, false);

  //Il LoginFormManager consente di inizializzare, nascondere o mostrare la form di login
  function LoginFormManager(loginForm){
      this.initialize=function(){
          loginForm.style.display="block";
      }
      this.hide=function(){
          loginForm.style.display="none";
          loginForm.reset();
      }
      this.show=function(){
          loginForm.style.display="block";
      }
  }

  //Il RegistrationFormManager consente di inizializzare, nascondere o mostrare la form di registrazione
  function RegistrationFormManager(registrationForm){
      this.initialize=function(){
          registrationForm.style.display="none";
      }
      this.hide=function(){
          registrationForm.style.display="none";
          registrationForm.reset();
      }
      this.show=function(){
          registrationForm.style.display="block";
      }
  }

  //L'ErrorContainerManager si occupa di mostrare o nascondere eventuali messaggi di errore
  function ErrorContainerManager(errorMessage){
      this.initialize=function(){
          errorMessage.style.display="none";
      }
      this.reset=function(){
          errorMessage.style.display="none";
          errorMessage.textContent="";
      }
      this.show=function(message){
          errorMessage.style.display="block";
          errorMessage.textContent=message;
      }
  }

  //Lo SwitchManager definisce come deve avvenire il cambiamento della pagina quando si clicca sul link per cambiare form
  function SwitchManager(link, loginFormManager,registrationFormManager, errorContainerManager){
      var status = "login";

      this.initialize = function(){
          link.textContent = "Registrati";
          link.addEventListener('click',(e)=>{
                e.preventDefault();
                if(status==="login"){
                    link.textContent="Login";
                    loginFormManager.hide();
                    registrationFormManager.show();
                    status = "registration";
                }
                else{
                    link.textContent = "Registrati";
                    loginFormManager.show();
                    registrationFormManager.hide();
                    status = "login";
                }
                errorContainerManager.reset();
          });
      }
  }


  function PageManager(){
      var errorContainerManager = new ErrorContainerManager(document.getElementById("divError"));
      var loginFormManager = new LoginFormManager(document.getElementById("formLogin"));
      var registrationFormManager = new RegistrationFormManager(document.getElementById("formRegistration"));
      var linkLogin = new SwitchManager(document.getElementById("loginLink"),loginFormManager,registrationFormManager,errorContainerManager);

      //Definizione dello stato iniziale della pagina
      this.start=function(){
          errorContainerManager.initialize();
          loginFormManager.initialize();
          registrationFormManager.initialize();
          linkLogin.initialize();
      }

      //Definizione del comportamento dell'operazione login
      document.getElementById("btnLogin").addEventListener('click', (e) => {
        var form = document.getElementById("formLogin");
        //Dopo aver verificato lato client la correttezza dei campi della form, viene richiesta al server la validazione delle credenziali
        if (form.checkValidity()) {
          asyncCall("POST", 'CheckLogin', form,
            function(request) {
              if (request.readyState === XMLHttpRequest.DONE) {
                var message = request.responseText;
                switch (request.status) {
                  //Operazione eseguita con successo
                  case HttpCodes.SUCCESS:
                    var messageJSON = JSON.parse(message);
                    sessionStorage.setItem('username', messageJSON.username);
                    //L'utente viene reindirizzato alla home page
                    window.location.href = "home.html";
                    //Quando l'operazione ha successo, eventuali errori precedentemente presenti vengono rimossi
                    errorContainerManager.reset();
                    break;
                  //Formato non adatto dei dati o parametri assenti
                  case HttpCodes.BAD_REQUEST:
                  //Credenziali errate
                  case HttpCodes.UNAUTHORIZED:
                  //Il server non è riuscito a completare l'operazione
                  case HttpCodes.SERVER_ERROR:
                    errorContainerManager.show(message);
                    break;
                }
              }
            }
          );
        } else {
            form.reportValidity();
        }
    });

    //Definizione del comportamento dell'operazione registrazione
    document.getElementById("btnRegister").addEventListener('click', (e) => {
      var form = document.getElementById("formRegistration");
      var passwordFields = form.querySelectorAll("input[type=password]");

      //Oltre a controllare la validità dei campi inseriti, si verifica che le password inserite coincidano e non siano state lasciate vuote
      if (form.checkValidity() && passwordFields[0].value!=="" && passwordFields[1].value!=="" && passwordFields[0].value===passwordFields[1].value) {
        //Se tutti i campi sono validi, si procede alla richiesta di registrazione presso il server
        asyncCall("POST", 'CreateUser', form,
          function(request) {
            if (request.readyState === XMLHttpRequest.DONE) {
              var message = request.responseText;
              switch (request.status) {
                //L'operazione ha avuto successo
                case HttpCodes.SUCCESS:
                  var messageJSON = JSON.parse(message);
                  sessionStorage.setItem('username', messageJSON.username);
                  //L'utente viene reindirizzato direttamente alla home page
                  window.location.href = "home.html";
                  //Quando l'operazione ha successo, eventuali errori precedentemente presenti vengono rimossi
                  errorContainerManager.reset();
                  break;
                //Formato non adatto dei dati o parametri assenti
                case HttpCodes.BAD_REQUEST:
                  errorContainerManager.show(message);
                  break;
                //Username già esistente
                case HttpCodes.CONFLICT:
                  errorContainerManager.show(message);
                  break;
                //Il server non è riuscito a completare l'operazione
                case HttpCodes.SERVER_ERROR:
                  errorContainerManager.show(message);
                  break;
              }
            }
          }
        );
      } else {
          if(passwordFields[0].value!=="" && passwordFields[1].value!=="" && passwordFields[0].value!==passwordFields[1].value){
              errorContainerManager.show("I campi password non coincidono");
          }
          form.reportValidity();
      }
    });
  }

})();
