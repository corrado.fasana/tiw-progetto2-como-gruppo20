
//Definisce le tipologie di codice HTTP che verranno utilizzati
  var HttpCodes = {
      SUCCESS : 200,
      RESET_CONTENT : 205,
      BAD_REQUEST: 400,
      UNAUTHORIZED: 401,
      FORBIDDEN: 403,
      CONFLICT: 409,
      SERVER_ERROR : 500
  }

//Effettua una chiamata asincrona al server
function asyncCall(method, url, formElement, callback) {
    var req = new XMLHttpRequest();
    req.onreadystatechange = function() {
      callback(req)
    };
    req.open(method, url);
    //Se non c'è alcuna form, viene inviata la richiesta normalmente
    if (formElement == null) {
      req.send();
    } else {    //In caso contrario, viene inviata anche la form come parametro
      req.send(new FormData(formElement));
    }
  }

  //Necessaria per terminare la sessione qualora l'oggetto session del server sia stato invalidato.
  //Si scatena l'evento logout e dopo di che, si reindirizza alla index.html
  function closeSession(){
      var logoutEvent = new Event('click');
      document.getElementById("btnLogout").dispatchEvent(logoutEvent);
      window.location.href="index.html";
  }
