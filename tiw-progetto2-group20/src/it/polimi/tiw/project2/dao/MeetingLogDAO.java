package it.polimi.tiw.project2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import it.polimi.tiw.project2.beans.MeetingLog;

public class MeetingLogDAO {
	private Connection connection;

	public MeetingLogDAO(Connection connection) {
		super();
		this.connection = connection;
	}
	
	/**
	 * Consente di ottenere la data di ultima modifica della tabella riunione
	 * @return un oggetto MeetingLog con la data trovata se questa � presente. Null altrimenti.
	 * @throws SQLException
	 */
	public MeetingLog getLastModified() throws SQLException {
		String query = "SELECT id, istante FROM log_riunione WHERE id>= ALL (SELECT MAX(id) FROM log_riunione)";
		
		try(PreparedStatement pStatement = connection.prepareStatement(query)){
			try(ResultSet result = pStatement.executeQuery()){
				if(result.next())
					return new MeetingLog(result.getInt("id"),result.getTimestamp("istante"));
			}
		}
		return null;
	}
}
