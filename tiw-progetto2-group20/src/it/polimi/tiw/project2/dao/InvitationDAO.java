package it.polimi.tiw.project2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class InvitationDAO {
	private Connection connection;

	public InvitationDAO(Connection connection) {
		super();
		this.connection = connection;
	}
	
	/**
	 * Consente di creare dei nuovi inviti
	 * @param idReunion -> id della riunione
	 * @param guestsIds -> id degli utenti da invitare alla riunione
	 * @throws SQLException
	 */
	public void createInvitations(int idReunion, List<Integer> guestsIds) throws SQLException {
		String query = "INSERT INTO invito (idriunione, idinvitato) VALUES (?,?)";
		
		try(PreparedStatement pStatement = connection.prepareStatement(query)){
			for(Integer guestId : guestsIds) {
				pStatement.setInt(1, idReunion);
				pStatement.setInt(2, guestId);
				
				pStatement.executeUpdate();
			}
		}
	}
}
