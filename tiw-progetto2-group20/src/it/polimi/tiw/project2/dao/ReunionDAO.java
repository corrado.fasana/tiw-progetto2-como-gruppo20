package it.polimi.tiw.project2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import it.polimi.tiw.project2.beans.Reunion;

public class ReunionDAO {
	private Connection connection;

	public ReunionDAO(Connection connection) {
		super();
		this.connection = connection;
	}
	
	/**
	 * Consente di creare una nuova riunione e di associargli i partecipanti
	 * @param reunion -> la riunione da creare (di cui viene settato l'id)
	 * @param guestsIds -> i partecipanti da invitare alla riunione
	 * @throws SQLException
	 */
	public void createReunion(Reunion reunion, List<Integer> guestsIds) throws SQLException{
		InvitationDAO iDAO = new InvitationDAO(connection);
		String query = "INSERT INTO riunione (titolo, dataora, durata, maxpartecipanti, organizzatore) VALUES (?,?,?,?,?) RETURNING id";	

		connection.setAutoCommit(false);
		try(PreparedStatement pStatement = connection.prepareStatement(query)){
			pStatement.setString(1, reunion.getTitle());
			pStatement.setTimestamp(2, reunion.getDateTime());
			pStatement.setInt(3, reunion.getDuration());
			pStatement.setInt(4, reunion.getMaxParticipants());
			pStatement.setInt(5, reunion.getIdPromoter());
			
			ResultSet result = pStatement.executeQuery();
			if(result.next()) {
				reunion.setId(result.getInt(1));
			}
			iDAO.createInvitations(reunion.getId(), guestsIds);
			connection.commit();
		}catch(SQLException exc) {
			//Se una delle due operazioni non va a buon fine, si fa una rollback
			connection.rollback();
			throw exc;
		}
		finally{
			connection.setAutoCommit(true);
		}
	}
	
	/**
	 * Consente di ottenere l'insieme delle riunioni indette da un utente, non ancora scadute
	 * @param userId -> id dell'utente
	 * @return -> lista delle riunioni non scadute
	 * @throws SQLException
	 */
	public List<Reunion> getNotExpiredReunionByUserId(int userId) throws SQLException{
		String query = "SELECT * FROM riunione WHERE organizzatore = ? AND dataora + (INTERVAL '1 min' * durata) >= ?";
		List<Reunion> reunions = new ArrayList<Reunion>();
		
		try(PreparedStatement pStatement = connection.prepareStatement(query)){
			pStatement.setInt(1, userId);
			//La scadenza � valutata rispetto all'istante attuale (i secondi non vengono presi in considerazione)
			pStatement.setTimestamp(2, Timestamp.valueOf(LocalDateTime.now().withSecond(0)));
			try(ResultSet result = pStatement.executeQuery()){
				while(result.next()) {
					Reunion reunion = new Reunion(result.getInt("id"),result.getString("titolo"),result.getTimestamp("dataora"),result.getInt("durata"),result.getInt("maxpartecipanti"),result.getInt("organizzatore"));
					reunions.add(reunion);
				}
			}
		}
		return reunions;
	}
	
	/**
	 * Consente di ottenere l'insieme delle riunioni a cui un utente � stato invitato, non ancora scadute
	 * @param userId -> id dell'utente invitato
	 * @return -> lista delle riunioni non scadute
	 * @throws SQLException
	 */
	public List<Reunion> getNotExpiredInvitedReunionByUserId(int userId) throws SQLException{
		String query = "SELECT * FROM riunione R, invito I WHERE R.id = I.idriunione AND I.idinvitato = ? AND R.dataora + (INTERVAL '1 min' * R.durata) >= ?";
		List<Reunion> reunions = new ArrayList<Reunion>();
		
		try(PreparedStatement pStatement = connection.prepareStatement(query)){
			pStatement.setInt(1, userId);
			//La scadenza � valutata rispetto all'istante attuale (i secondi non vengono presi in considerazione)
			pStatement.setTimestamp(2, Timestamp.valueOf(LocalDateTime.now().withSecond(0)));
			try(ResultSet result = pStatement.executeQuery()){
				while(result.next()) {
					Reunion reunion = new Reunion(result.getInt("id"),result.getString("titolo"),result.getTimestamp("dataora"),result.getInt("durata"),result.getInt("maxpartecipanti"),result.getInt("organizzatore"));
					reunions.add(reunion);
				}
			}
		}
		return reunions;
	}
}
