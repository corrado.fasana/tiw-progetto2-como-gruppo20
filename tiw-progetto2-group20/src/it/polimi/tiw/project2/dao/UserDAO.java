package it.polimi.tiw.project2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import it.polimi.tiw.project2.beans.User;

public class UserDAO {
	private Connection connection;

	public UserDAO(Connection connection) {
		super();
		this.connection = connection;
	}
	
	/**
	 * Consente di ottenere tutti gli utenti del DB, ad esclusione di quello indicato
	 * @param userId -> id dell'utente in considerazione
	 * @return -> una lista degli altri utenti del DB
	 * @throws SQLException
	 */
	public List<User> getAllOtherUsers(int userId) throws SQLException{
		String query = "SELECT * FROM utente WHERE id != ?";
		List<User> users = new ArrayList<User>();
		try(PreparedStatement pStatement = connection.prepareStatement(query)){
			pStatement.setInt(1, userId);
			try(ResultSet result = pStatement.executeQuery()){
				while(result.next()) {
					User user = new User(result.getInt("id"),result.getString("nome"),result.getString("cognome"),result.getString("username"),result.getString("email"));
					users.add(user);
				}
			}
		}
		return users;
	}
	
	/**
	 * Verifica che le credenziali inserite dall'utente siano corrette
	 * @param username -> username dell'utente
	 * @param password -> password di accesso
	 * @return -> null se le credenziali sono errate o non esiste alcun utente con quello username
	 * 			-> uno User altrimenti
	 * @throws SQLException
	 */
	public User checkCredentials(String username, String password) throws SQLException {
		String query = "SELECT * FROM utente WHERE username = ? AND password = ?";
		User user = null;
		
		try(PreparedStatement pStatement = connection.prepareStatement(query)){
			pStatement.setString(1, username);
			pStatement.setString(2, password);
			try(ResultSet result = pStatement.executeQuery()){
				if(result.isBeforeFirst()) {
					result.next();
					user = new User(result.getInt("id"),result.getString("nome"),result.getString("cognome"),result.getString("username"), result.getString("email"));
				}
			}
		}
		return user;
	}
	
	/**
	 * Consente di creare un nuovo utente
	 * @param name -> Nome
	 * @param surname -> Cognome
	 * @param username -> Username
	 * @param password -> Password
	 * @return -> l'utente creato se l'operazione � andata a buon fine
	 * 			-> null se lo username indicato � gi� esistente
	 * @throws SQLException
	 */
	public User createUser(String name, String surname, String username, String password, String email) throws SQLException{
		String query1 = "SELECT 1 FROM utente WHERE username = ?";	//Query di verifica dell'univocit� dello username
		String query2 = "INSERT INTO utente (nome, cognome, username, password, email) VALUES (?,?,?,?,?) RETURNING id";
		User user = null;
		
		try(PreparedStatement pStatement = connection.prepareStatement(query1)){
			pStatement.setString(1,username);
			try(ResultSet result = pStatement.executeQuery()){
				//Se viene ritornato qualcosa, allora lo username � gi� esistente
				if(result.isBeforeFirst()) {
					return null;
				}
			}
		}
		
		//Altrimenti, si effettua l'inserimento del nuovo utente nel DB, restituendo l'utente appena creato
		try(PreparedStatement pStatement = connection.prepareStatement(query2)){
			pStatement.setString(1, name);
			pStatement.setString(2, surname);
			pStatement.setString(3, username);
			pStatement.setString(4, password);
			pStatement.setString(5, email);
			try(ResultSet result = pStatement.executeQuery()){
				if(result.isBeforeFirst()) {
					result.next();
					user = new User(result.getInt(1),name,surname,username,email);
				}
			}
		}
		return user;
	}
}
