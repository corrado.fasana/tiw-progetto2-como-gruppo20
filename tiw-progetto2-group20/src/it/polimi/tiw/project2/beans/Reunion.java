package it.polimi.tiw.project2.beans;

import java.sql.Timestamp;

//Bean relativo alla tabella 'riunione' del DB
public class Reunion {
	private int id;					//Id univoco
	private String title;			//Titolo
	private Timestamp dateTime;		//Data e ora della riunione
	private int duration;			//Durata in minuti
	private int maxParticipants;	//Numero massimo di partecipanti (escluso l'organizzatore)
	private int idPromoter;			//Id dell'organizzatore della riunione
	
	public Reunion(int id,String title, Timestamp dateTime, int duration, int maxParticipants, int idPromoter) {
		super();
		this.id=id;
		this.title = title;
		this.dateTime = dateTime;
		this.duration = duration;
		this.maxParticipants = maxParticipants;
		this.idPromoter = idPromoter;
	}
	
	public Reunion(String title, Timestamp dateTime, int duration, int maxParticipants, int idPromoter) {
		super();
		this.id=0;
		this.title = title;
		this.dateTime = dateTime;
		this.duration = duration;
		this.maxParticipants = maxParticipants;
		this.idPromoter = idPromoter;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Timestamp getDateTime() {
		return dateTime;
	}

	public void setDateTime(Timestamp dateTime) {
		this.dateTime = dateTime;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public int getMaxParticipants() {
		return maxParticipants;
	}

	public void setMaxParticipants(int maxPartecipants) {
		this.maxParticipants = maxPartecipants;
	}

	public int getIdPromoter() {
		return idPromoter;
	}

	public void setIdPromoter(int idPromoter) {
		this.idPromoter = idPromoter;
	}
}
