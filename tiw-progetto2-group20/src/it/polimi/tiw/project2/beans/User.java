package it.polimi.tiw.project2.beans;

//Bean relativo alla tabella 'utente' del DB
public class User {
	private int id;				//Id univoco
	private String name;		//Nome
	private String surname;		//Cognome
	private String username;	//Username (univoco)
	private String email; 		//Email
	
	public User(int id,String name, String surname, String username, String email) {
		super();
		this.id=id;
		this.name = name;
		this.surname = surname;
		this.username = username;
		this.email=email;
	}

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
