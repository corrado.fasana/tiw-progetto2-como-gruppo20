package it.polimi.tiw.project2.beans;

//Bean relativo alla tabella 'invito' del DB
public class Invitation {
	private int IdGuest;	//Id dell'invitato
	private int IdReunion;	//Id della riunione
	
	public Invitation(int idGuest, int idReunion) {
		super();
		IdGuest = idGuest;
		IdReunion = idReunion;
	}

	public int getIdGuest() {
		return IdGuest;
	}

	public void setIdGuest(int idGuest) {
		IdGuest = idGuest;
	}

	public int getIdReunion() {
		return IdReunion;
	}

	public void setIdReunion(int idReunion) {
		IdReunion = idReunion;
	}	
}
