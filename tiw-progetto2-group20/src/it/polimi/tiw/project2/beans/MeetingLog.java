package it.polimi.tiw.project2.beans;

import java.sql.Timestamp;

//Bean relativo alla tabella 'log_riunione' del DB
public class MeetingLog {
	private int id;					//id
	private Timestamp timestamp;	//istante di modifica della tabella 'riunione'
	
	public MeetingLog(int id, Timestamp timestamp) {
		super();
		this.id = id;
		this.timestamp = timestamp;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public Timestamp getTimestamp() {
		return timestamp;
	}
	
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}
}
