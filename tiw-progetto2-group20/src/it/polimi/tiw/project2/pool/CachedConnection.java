package it.polimi.tiw.project2.pool;

import java.sql.Connection;

public class CachedConnection {
	private Connection connection;
	private boolean inUse = false;
	
	public CachedConnection(Connection connection) {
		this.connection=connection;
	}
	
	public Connection getConnection() {
		return connection;
	}
	
	public boolean isInUse() {
		return inUse;
	}
	
	public void setInUse(boolean inUse) {
		this.inUse = inUse;
	}
}
