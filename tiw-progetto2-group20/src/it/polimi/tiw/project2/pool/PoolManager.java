package it.polimi.tiw.project2.pool;

import java.sql.Connection;
import java.sql.SQLException;

import it.polimi.tiw.project2.pool.ConnectionPool;
import it.polimi.tiw.project2.pool.PoolManager;

public class PoolManager {
	private static PoolManager instance;	//Istanza del singleton
	private static ConnectionPool pool;		//Riferimento al pool
	private static final int MAX_CONNECTIONS = 20;	//Numero massimo di connessioni consentite
	private static int clients=0;
	
	private PoolManager(String url, String user, String pwd, String driver) throws ClassNotFoundException {
		Class.forName(driver);
		pool = new ConnectionPool(url, user, pwd, MAX_CONNECTIONS);
	}
	
	/**
	 * Ritorna un'istanza del pool manager
	 * @param url -> url del DB
	 * @param user -> user del DB
	 * @param pwd	-> password di accesso al DB
	 * @param driver -> driver da utilizzare
	 * @return -> un'istanza del manager
	 * @throws ClassNotFoundException
	 */
	public static synchronized PoolManager getInstance(String url, String user, String pwd, String driver) throws ClassNotFoundException {
		if(instance==null) {
			instance=new PoolManager(url, user, pwd, driver);
		}
		clients++;
		return instance;
	}
	
	/**
	 * Restituisce una connessione al DB
	 * @return -> una Connection al DB
	 * @throws SQLException
	 */
	public Connection getConnection() throws SQLException {
        return pool.getConnection();
    }
	
	/**
	 * Consente di restituire una connessione al pool
	 * @param connection -> Connection da rilasciare
	 */
	public void releaseConnection(Connection connection){		
		pool.releaseConnection(connection);
	}
	
	/**
	 * Rilascia l'istanza del poolManager: se il numero di clienti che possiedono l'istanza � 0, vengono chiuse tutte le connessioni al DB
	 * @throws SQLException
	 */
	public synchronized void releasePoolManager() throws SQLException {
		clients--;
		if(clients!=0) return;
		else {
			pool.releaseAll();
		}
	}
}
