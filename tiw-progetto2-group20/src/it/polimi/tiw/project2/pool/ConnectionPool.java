package it.polimi.tiw.project2.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Vector;

import it.polimi.tiw.project2.pool.CachedConnection;

public class ConnectionPool {
	private final int MAX_NUMBER_OF_CONNECTIONS;
	
	private int createdConnections=0;	//Numero di connessioni attive al momento
	private Vector<CachedConnection> cachedConnections = new Vector<CachedConnection>();	//Insieme delle connessioni attive
	
	private String dbUrl;	//Url del database
	//Credenziali di accesso al DB
	private String dbPassword;
	private String dbUser;
	
	public ConnectionPool(String url, String user, String password, int maxConnections) {
		dbUrl=url;
		dbUser=user;
		dbPassword=password;
		MAX_NUMBER_OF_CONNECTIONS=maxConnections;
	}
	
	/**
	 * Consente di ottenere una connessione al DB.
	 * 1) Se � presente una connessione attiva libera, questa viene restituita;
	 * 2) Se non ci sono connessioni libere ma il numero di connessioni attive � inferiore al massimo consentito,
	 * 	  ne viene creata una e viene restituita;
	 * 3) In caso contrario, il pool ripete tutto in attesa che si liberi una connessione. Appena ci� accade, questa viene restituita
	 * @return -> una connessione al database
	 * @throws SQLException
	 */
	public Connection getConnection() throws SQLException {
		CachedConnection cachedConnection = null;
		
		while(true) {
			synchronized(this) {
				for(int i=0; i<createdConnections;i++)
					if(!cachedConnections.elementAt(i).isInUse()) {
						cachedConnection=cachedConnections.elementAt(i);
						break;
					}
				if(cachedConnection!=null) {
					cachedConnection.setInUse(true);
					break;
				}
				else if(createdConnections<MAX_NUMBER_OF_CONNECTIONS-1){
					Connection newConnection = DriverManager.getConnection(dbUrl, dbUser, dbPassword);
					cachedConnection = new CachedConnection(newConnection);
					cachedConnection.setInUse(true);
					cachedConnections.add(cachedConnection);
					createdConnections++;
					break;
				}
			}
		}
		Connection connection = cachedConnection.getConnection();
		if(!connection.isValid(0)) {
			connection.close();
			connection = DriverManager.getConnection(dbUrl, dbUser, dbPassword);
		}
		return connection;
	}
	
	/**
	 * Consente di restituire una connessione al database
	 * @param connection
	 */
	public synchronized void releaseConnection(Connection connection){
		for(int i=0;i<createdConnections;i++)
			if(cachedConnections.elementAt(i).getConnection()==connection) {
				cachedConnections.elementAt(i).setInUse(false);
				return;
			}
	}

	/**
	 * Consente di chiudere tutte le connessioni attive
	 * @throws SQLException
	 */
	public synchronized void releaseAll() throws SQLException{
		for(int i=0; i<createdConnections;i++) {
			CachedConnection connection = cachedConnections.elementAt(i);
			if(!(connection.getConnection().isClosed())) {
				connection.getConnection().close();
			}
		}
		createdConnections = 0;
		cachedConnections.clear();
	}	
}
