package it.polimi.tiw.project2.controllers;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringEscapeUtils;

import it.polimi.tiw.project2.beans.Reunion;
import it.polimi.tiw.project2.beans.User;

/**
 * Servlet implementation class CheckReunionParameters
 */
@WebServlet("/CheckReunionParameters")
@MultipartConfig
public class CheckReunionParameters extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CheckReunionParameters() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		String title = StringEscapeUtils.escapeJava(request.getParameter("title"));
		String strMaxParticipants = request.getParameter("maxParticipants");
		String strDuration = request.getParameter("duration");
		String strDateTime = request.getParameter("dateTime");
		String errors="";
		
		int maxParticipants=0;
		int duration=0;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
		Timestamp dateTime=null;
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");

		if(title==null || strMaxParticipants==null || strDuration == null || strDateTime==null || title.isEmpty() || strMaxParticipants.isEmpty() || strDuration.isEmpty() || strDateTime.isEmpty())
		{
			errors = "Tutti i campi devono essere riempiti"+System.lineSeparator();
		}
		else {
			try {
				maxParticipants = Integer.parseInt(request.getParameter("maxParticipants"));
				duration = Integer.parseInt(request.getParameter("duration"));
				dateTime = Timestamp.valueOf(LocalDateTime.parse(strDateTime, formatter));
				
				if(maxParticipants<=0 || duration <=0 || dateTime.before(Timestamp.valueOf(LocalDateTime.now()))){
					errors = "I valori forniti per alcuni dei campi non sono accettabili"+System.lineSeparator();
				}
			}catch(NumberFormatException | DateTimeParseException e) {
				errors = "Formato dei dati non corretto"+System.lineSeparator();
			}
		}
		
		if(!errors.isEmpty()) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println(errors);
			return;
		}
		
		//Se tutti i controlli vengono superati, viene creata una riunione temporaneamente salvata in sessione
		Reunion reunion = new Reunion(title, dateTime, duration, maxParticipants, ((User)session.getAttribute("user")).getId());
		session.setAttribute("reunion", reunion);
		session.removeAttribute("attempts");
		response.setStatus(HttpServletResponse.SC_OK);
		//Si reinvia al client il numero massimo di partecipanti perch� lo salvi nel sessionStorage
		response.getWriter().print(maxParticipants);
	}
}
