package it.polimi.tiw.project2.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import it.polimi.tiw.project2.beans.User;
import it.polimi.tiw.project2.dao.UserDAO;
import it.polimi.tiw.project2.pool.PoolManager;

/**
 * Servlet implementation class GetUsers
 */
@WebServlet("/GetUsers")
@MultipartConfig
public class GetUsers extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private PoolManager poolManager;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetUsers() {
        super();
    }

    public void init() throws ServletException,UnavailableException {
    	ServletContext sCtx = getServletContext();
    	String url = sCtx.getInitParameter("dbUrl");
    	String user = sCtx.getInitParameter("dbUser");
    	String password = sCtx.getInitParameter("dbPassword");
    	String driver = sCtx.getInitParameter("dbDriver");
		try {
			poolManager = PoolManager.getInstance(url, user, password, driver);
		} catch (ClassNotFoundException e) {
			throw new UnavailableException("Couldn't load database driver");
		}
	}
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = (User) request.getSession().getAttribute("user");
		Connection connection = null;
		
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		try {
			connection = poolManager.getConnection();
		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().println("Impossibile connettersi al database. Riprovare pi� tardi.");
			return;
		}
		
		UserDAO uDAO = new UserDAO(connection); 
		
		List<User> users = new ArrayList<User>();
		try {
			users= uDAO.getAllOtherUsers(user.getId());
		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().println("Impossibile ottenere la lista degli utenti. Riprovare pi� tardi.");
			return;
		}finally {
			poolManager.releaseConnection(connection);
		}
		
		Gson gson = new GsonBuilder().create();
		String jsonUsers = gson.toJson(users);
		response.setStatus(HttpServletResponse.SC_OK);
		response.getWriter().write(jsonUsers);
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
	}

}
