package it.polimi.tiw.project2.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import it.polimi.tiw.project2.dao.ReunionDAO;
import it.polimi.tiw.project2.beans.Reunion;
import it.polimi.tiw.project2.beans.User;
import it.polimi.tiw.project2.pool.PoolManager;

/**
 * Servlet implementation class GetCreatedReunions
 */
@WebServlet("/GetCreatedReunions")
@MultipartConfig
public class GetCreatedReunions extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private PoolManager poolManager;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetCreatedReunions() {
        super();
    }

    public void init() throws ServletException,UnavailableException {
    	ServletContext sCtx = getServletContext();
    	String url = sCtx.getInitParameter("dbUrl");
    	String user = sCtx.getInitParameter("dbUser");
    	String password = sCtx.getInitParameter("dbPassword");
    	String driver = sCtx.getInitParameter("dbDriver");
		try {
			poolManager = PoolManager.getInstance(url, user, password, driver);
		} catch (ClassNotFoundException e) {
			throw new UnavailableException("Couldn't load database driver");
		}
	}
    
    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		User loggedUser = (User) session.getAttribute("user");
		Connection connection = null;
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		try {
			connection = poolManager.getConnection();
		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().println("Impossibile connettersi al database. Riprovare pi� tardi.");
			return;
		}
		
		ReunionDAO rDAO = new ReunionDAO(connection);
		List<Reunion> createdReunions = new ArrayList<Reunion>();
		
		try {
			createdReunions=rDAO.getNotExpiredReunionByUserId(loggedUser.getId());
		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().println("Impossibile ottenere le riunioni create. Riprovare pi� tardi.");
			return;
		}
		finally {
			poolManager.releaseConnection(connection);
		}
		
		Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").create();
		String jsonReunions = gson.toJson(createdReunions);
		response.setStatus(HttpServletResponse.SC_OK);
		response.getWriter().write(jsonReunions);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	public void destroy() {
		try {
			poolManager.releasePoolManager();
		} catch (SQLException e) {
		}
	}
}
