package it.polimi.tiw.project2.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import it.polimi.tiw.project2.beans.Reunion;

/**
 * Servlet implementation class CreateReunion
 */
@WebServlet("/CheckNumberOfParticipants")
@MultipartConfig
public class CheckNumberOfParticipants extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final int MAX_ATTEMPTS = 3;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CheckNumberOfParticipants() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		int maxParticipants = ((Reunion)session.getAttribute("reunion")).getMaxParticipants();
		String[] selectedUsers = request.getParameterValues("selectedUsers");
		List<Integer> preselectedUsers = new ArrayList<Integer>();
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		//Controllo parametri lato server
		//Controllo che venga selezionato almeno un utente
		if(selectedUsers==null) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			response.getWriter().println("La riunione richiede la presenza di almeno un partecipante");
			return;
		}
				
		//Conversione degli id degli utenti selezionati, da String a Integer
		for(int i=0; i<selectedUsers.length;i++) {
			try {
				preselectedUsers.add(Integer.parseInt(selectedUsers[i]));
			}catch(NumberFormatException e) {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				response.getWriter().println("Formato dei dati non corretto");
				return;
			}
		}
		
		//Se il numero di utenti selezionati eccede il massimo consentito si segnala un errore
		if(preselectedUsers.size()>maxParticipants) {
			int attempts = (session.getAttribute("attempts")==null)?1:((int)session.getAttribute("attempts"))+1;
			if(attempts<MAX_ATTEMPTS) {
				String errorMessage = "Troppi utenti selezionati, eliminane almeno "+(preselectedUsers.size()-maxParticipants);
				session.setAttribute("attempts", attempts);
				response.setStatus(HttpServletResponse.SC_FORBIDDEN);
				response.getWriter().println(errorMessage);
			}
			else {
				session.removeAttribute("attempts");
				session.removeAttribute("reunion");
				response.setStatus(HttpServletResponse.SC_RESET_CONTENT);
				return;
			}
		}
		
		//Se tutto � corretto, si inoltra la richiesta alla servlet che si occupa di creare la riunione
		else {
			request.setAttribute("guestsIds", preselectedUsers);
			request.getRequestDispatcher("/CreateReunion").forward(request, response);
		}
	}


}
