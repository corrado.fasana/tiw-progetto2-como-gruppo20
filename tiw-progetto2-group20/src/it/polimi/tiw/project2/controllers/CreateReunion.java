package it.polimi.tiw.project2.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import it.polimi.tiw.project2.beans.Reunion;
import it.polimi.tiw.project2.dao.ReunionDAO;
import it.polimi.tiw.project2.pool.PoolManager;

/**
 * Servlet implementation class CreateReunion
 */
@WebServlet("/CreateReunion")
@MultipartConfig
public class CreateReunion extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private PoolManager poolManager;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateReunion() {
        super();
        // TODO Auto-generated constructor stub
    }

    public void init() throws ServletException,UnavailableException {
    	ServletContext sCtx = getServletContext();
    	String url = sCtx.getInitParameter("dbUrl");
    	String user = sCtx.getInitParameter("dbUser");
    	String password = sCtx.getInitParameter("dbPassword");
    	String driver = sCtx.getInitParameter("dbDriver");
		try {
			poolManager = PoolManager.getInstance(url, user, password, driver);
		} catch (ClassNotFoundException e) {
			throw new UnavailableException("Couldn't load database driver");
		}
	}
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Connection connection = null;
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		try {
			connection = poolManager.getConnection();
		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().println("Impossibile connettersi al database. Riprovare pi� tardi.");
			return;
		}
		
		ReunionDAO rDAO = new ReunionDAO(connection);
		Reunion reunion = (Reunion) session.getAttribute("reunion");
		
		//Creazione della riunione
		try {
			rDAO.createReunion(reunion,(List<Integer>)request.getAttribute("guestsIds"));
		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().println("Impossibile creare la riunione. Riprovare pi� tardi.");
			return;
		}
		finally {
			poolManager.releaseConnection(connection);
		}
		
		//Gli attributi prima salvati in sessione vengono cancellati perch� non pi� utili
		session.removeAttribute("reunion");
		response.setStatus(HttpServletResponse.SC_OK);
	}

	public void destroy() {
		try {
			poolManager.releasePoolManager();
		} catch (SQLException e) {
		}
	}
}
