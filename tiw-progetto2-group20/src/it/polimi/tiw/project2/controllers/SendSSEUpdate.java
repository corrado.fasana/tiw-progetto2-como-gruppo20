package it.polimi.tiw.project2.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import it.polimi.tiw.project2.beans.MeetingLog;
import it.polimi.tiw.project2.beans.Reunion;
import it.polimi.tiw.project2.beans.User;
import it.polimi.tiw.project2.dao.MeetingLogDAO;
import it.polimi.tiw.project2.dao.ReunionDAO;
import it.polimi.tiw.project2.pool.PoolManager;

/**
 * Servlet implementation class SendSSEUpdate
 * 
 * Quando avviene la notifica al client?
 *  1) Quando si rileva un cambiamento nel n� di riunioni create dall'utente non ancora scadute (es. scade una riunione)
 *  2) Quando si rileva un cambiamento nel n� di riunioni a cui l'utente � stato invitato, non ancora scadute (es. scade un invito)
 *  3) Quando la tabella riunione � stata aggiornata. Questo serve per evitare che verificandosi nel medesimo intervallo di valutazione
 *     una scadenza e un nuovo invito, non venga rilevato alcun cambiamento nel n� di riunioni e non si notifichi nulla al client 
 */
@WebServlet("/SendSSEUpdate")
@MultipartConfig
public class SendSSEUpdate extends HttpServlet {

	private static final long serialVersionUID = 1L;
    private PoolManager poolManager;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SendSSEUpdate() {
        super();
    }

    public void init() throws ServletException,UnavailableException {
    	ServletContext sCtx = getServletContext();
    	String url = sCtx.getInitParameter("dbUrl");
    	String user = sCtx.getInitParameter("dbUser");
    	String password = sCtx.getInitParameter("dbPassword");
    	String driver = sCtx.getInitParameter("dbDriver");
		try {
			poolManager = PoolManager.getInstance(url, user, password, driver);
		} catch (ClassNotFoundException e) {
			throw new UnavailableException("Couldn't load database driver");
		}
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Connection connection = null;
		HttpSession session = request.getSession(false);
		PrintWriter pw = response.getWriter();
		
		int userId = ((User)(session.getAttribute("user"))).getId();
		
		//Si in posta un flusso di streaming da server a client
		response.setContentType("text/event-stream, charset=UTF-8");
		
		/*Se l'attributo alreadyActive non � null, allora esiste gi� una connessione SSE per l'invio di dati al client.
		  Perci�, non serve fare nulla*/
		if(session.getAttribute("alreadyActive")!=null) {
			return;
		}
		//In caso contrario, si imposta la variabile di sessione: da ora in poi esiste una connessione SSE per l'invio di dati al client
		else session.setAttribute("alreadyActive", true);
             
	    while(true)
	    {
	    	try {
		    	//Se la sessione � null, non serve pi� inviare dati al client
		    	if(request.getSession(false)==null)
		        	break;
		    	try {
					//La connessione viene instaurata all'inizio della procedura e rilasciata una volta scaduta la sessione
					connection = poolManager.getConnection();
				} catch (SQLException e) {
					pw.write("event: error\n");
		        	pw.write("data: "+ "Couldn't connect to database" + "\n\n");
		            pw.flush();
					return;
				}
		    	
		    	MeetingLogDAO mlDAO = new MeetingLogDAO(connection);
		    	Timestamp lastModified = null;
		    	Timestamp lastModifiedAttr = (Timestamp) session.getAttribute("lastModified");
		    	Integer lastCreatedReunionsCounter = (Integer) session.getAttribute("createdReunionsCounter");
		    	Integer lastInvitedReunionsCounter = (Integer) session.getAttribute("invitedReunionsCounter");
		    			    	
		    	try {
					MeetingLog result = mlDAO.getLastModified();
					if(result!=null) 
						lastModified = result.getTimestamp();
				} catch (SQLException e) {
					e.printStackTrace();
					poolManager.releaseConnection(connection);
					pw.write("event: error\n");
		        	pw.write("data: "+ "Couldn't get logs" + "\n\n");
		            pw.flush();
					return;
				}
		    	
		    	
		    	boolean send = (lastModifiedAttr==null || (lastModified!=null && !lastModified.equals(lastModifiedAttr)));
		    	if(lastModified!=null) session.setAttribute("lastModified", lastModified);
				
		    	ReunionDAO rDAO = new ReunionDAO(connection);
				List<Reunion> createdReunions = new ArrayList<Reunion>(),invitedReunions= new ArrayList<Reunion>();
				try {
					createdReunions.clear();
					createdReunions = rDAO.getNotExpiredReunionByUserId(userId);
				} catch (SQLException e) {
					e.printStackTrace();
					pw.write("event: error\n");
		        	pw.write("data: "+ "Couldn't get created reunions" + "\n\n");
		            pw.flush();
		            poolManager.releaseConnection(connection);
					return;
				}
				finally {
					poolManager.releaseConnection(connection);
				}
				
				if(lastCreatedReunionsCounter==null || createdReunions.size()!=lastCreatedReunionsCounter || send) {
					session.setAttribute("createdReunionsCounter",createdReunions.size());
					Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").create();
					String jsonReunions = gson.toJson(createdReunions);
					//Invio evento reunionCreated al client
					pw.write("event: reunionCreated\n");
			    	pw.write("data: "+ jsonReunions + "\n\n");
			        pw.flush();
				}
			
				try {
					invitedReunions = rDAO.getNotExpiredInvitedReunionByUserId(userId);
				} catch (SQLException e) {
					pw.write("event: error\n");
		        	pw.write("data: "+ "Couldn't get invited reunions" + "\n\n");
		            pw.flush();
					return;
				}
				
				if(lastInvitedReunionsCounter==null || invitedReunions.size()!=lastInvitedReunionsCounter || send) {
					session.setAttribute("invitedReunionsCounter",invitedReunions.size());
					Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").create();
					String jsonReunions = gson.toJson(invitedReunions);
					//Invio evento reunionInvitation al client
					pw.write("event: reunionInvitation\n");
			    	pw.write("data: "+ jsonReunions + "\n\n");
			        pw.flush();
				}
	    	}catch(IllegalStateException e){
	    		pw.close();
	    		return;
	    	}
	    }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	public void destroy() {
		try {
			poolManager.releasePoolManager();
		} catch (SQLException e) {
		}
	}
}
