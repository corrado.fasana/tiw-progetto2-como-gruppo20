package it.polimi.tiw.project2.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.validator.routines.EmailValidator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import it.polimi.tiw.project2.beans.User;
import it.polimi.tiw.project2.dao.UserDAO;
import it.polimi.tiw.project2.pool.PoolManager;

/**
 * Servlet implementation class CreateUser
 */
@WebServlet("/CreateUser")
@MultipartConfig
public class CreateUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private PoolManager poolManager;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateUser() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    public void init() throws ServletException,UnavailableException {
    	ServletContext sCtx = getServletContext();
    	String url = sCtx.getInitParameter("dbUrl");
    	String user = sCtx.getInitParameter("dbUser");
    	String password = sCtx.getInitParameter("dbPassword");
    	String driver = sCtx.getInitParameter("dbDriver");
    	
		try {
			poolManager = PoolManager.getInstance(url, user, password, driver);
		} catch (ClassNotFoundException e) {
			throw new UnavailableException("Couldn't load database driver");
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = StringEscapeUtils.escapeJava(request.getParameter("username"));
		String password = StringEscapeUtils.escapeJava(request.getParameter("password"));
		String repeatPassword = StringEscapeUtils.escapeJava(request.getParameter("repeatPassword"));
		String name = StringEscapeUtils.escapeJava(request.getParameter("name"));
		String surname = StringEscapeUtils.escapeJava(request.getParameter("surname"));
		String email = StringEscapeUtils.escapeJava(request.getParameter("email"));
		String errors ="";
		Connection connection = null;
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		//Controllo parametri lato server
		if(username==null || password==null || repeatPassword==null || name==null || surname==null || email==null || username.isEmpty() || password.isEmpty() || repeatPassword.isEmpty() || name.isEmpty() || surname.isEmpty() || email.isEmpty())
		{
			errors+="Tutti i campi devono essere riempiti"+System.lineSeparator();
		}
		else{
			if(!password.equals(repeatPassword)) {
				errors+="I campi password non coincidono"+System.lineSeparator();
			}
			if(!EmailValidator.getInstance().isValid(email)) {
				errors+="Il formato del campo email non � corretto";
			}
		}
		
		if(!errors.isEmpty()) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().println(errors);
			return;
		}
		
		try {
			connection = poolManager.getConnection();
		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().println("Impossibile connettersi al database. Riprovare pi� tardi.");
			return;
		}
		
		UserDAO uDAO = new UserDAO(connection);
		User user=null;
		try {
			user = uDAO.createUser(name, surname, username, password,email);
		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().println("Impossibile creare l'utente. Riprovare pi� tardi.");
			return;
		}finally {
			poolManager.releaseConnection(connection);
		}
			
		//Se l'utente � null, lo username � gi� esistente
		if(user==null) {
			response.setStatus(HttpServletResponse.SC_CONFLICT);
			response.getWriter().println("Username gi� esistente. Sceglierne un altro.");
		}
		else {
			HttpSession session = request.getSession(true);
			session.setAttribute("user", user);
			session.setMaxInactiveInterval(0);
			Gson gson = new GsonBuilder().create();
			String jsonUser = gson.toJson(user);
			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().write(jsonUser);
		}
	}

	public void destroy() {
		try {
			poolManager.releasePoolManager();
		} catch (SQLException e) {
		}
	}
}
